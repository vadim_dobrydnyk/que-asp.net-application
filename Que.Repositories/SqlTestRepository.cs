﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Que.Entities;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Que.Repositories
{
    public class SqlTestRepository : ITestRepository 
    {
        private string _connectionString;

        private string _querySelectUserIdByEmail = "SELECT [Id] FROM tblUsers WHERE [Email] = @email";
        private string _quertySelectAllTests = @"SELECT [Id],[TestName] FROM tblTests";
        private string _querySelectTestById = @"SELECT [Id],[TestName] FROM tblTests WHERE Id = @idTest";
        private string _quertySelectQuestionsByTestId = @"SELECT[Id],[IdTest],[Title] FROM tblQuestions WHERE [IdTest] = @idTest";
        private string _quertySelectOptionsByQuestionId = @"SELECT[Id],[IdQuestion],[Title] FROM tblOptions WHERE [IdQuestion] = @idQuestion";
        private string _queryInsertResutTesting = @"INSERT INTO tblUsersAndResults ([IdUser], [IdTest], [TestResult], [DateOfResult]) VALUES (@idUser, @idTest, @testResult, @DateOfResult)";
        private string _queryIsOptionCorrect = @"SELECT [IsCorect] FROM tblOptions WHERE [Id] = @idOption";
        private string _querySelectUserResultsByUserId = @"SELECT [Id],[IdUser],[IdTest],[TestResult],[DateOfResult] FROM tblUsersAndResults WHERE [IdUser] = @idUser";
        private string _queryGetUserByEmail = @"SELECT [Id],[FirstName],[LastName],[Email],[Password] FROM tblUsers WHERE [Email] = @email";

        public SqlTestRepository(string stringConnection)
        {
            _connectionString = stringConnection;
        }

        public List<TestEntity> GetAllTests()
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_quertySelectAllTests, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        var selectedTests = new List<TestEntity>();
                        while (reader.Read())
                        {
                            selectedTests.Add(new TestEntity()
                            {
                                Id = (int)reader["Id"],
                                Title = (string)reader["TestName"]
                            });
                        }
                        return selectedTests;
                    }
                }
            }
        }

        public TestEntity GetTestById(int idTest)
        {
            TestEntity selectedTest = new TestEntity();

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_querySelectTestById, connection))
                {
                    command.Parameters.Add("@idTest", SqlDbType.Int).Value = idTest;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        selectedTest.Id = (int)reader["Id"];
                        selectedTest.Title = (string)reader["TestName"];
                    }
                }
            }
            List<QuestionEntity> selectedQuestions = GetQuestionsByTestId(idTest);
            foreach(var q in selectedQuestions)
            {
                q.Options = GetOptionsByQuestionId(q.Id);
            }
            selectedTest.Questions = selectedQuestions;
            return selectedTest;
        }
        public List<QuestionEntity> GetQuestionsByTestId(int idTest)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_quertySelectQuestionsByTestId, connection))
                {
                    command.Parameters.Add("@idTest", SqlDbType.Int).Value = idTest;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        var selectedQuestions = new List<QuestionEntity>();
                        while (reader.Read())
                        {
                            selectedQuestions.Add(new QuestionEntity()
                            {
                                Id = (int)reader["id"],
                                IdTest = (int)reader["IdTest"],
                                Title = (string)reader["Title"]
                            });
                        }
                        return selectedQuestions;
                    }
                }
            }
        }
        public List<OptionEntity> GetOptionsByQuestionId(int idQuestion)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_quertySelectOptionsByQuestionId, connection))
                {
                    command.Parameters.Add("@idQuestion", SqlDbType.Int).Value = idQuestion;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        var selectedOptions = new List<OptionEntity>();
                        while (reader.Read())
                        {
                            selectedOptions.Add(new OptionEntity()
                            {
                                Id = (int)reader["id"],
                                IdQuestion = (int)reader["IdQuestion"],
                                Title = (string)reader["Title"]
                            });
                        }
                        return selectedOptions;
                    }
                }
            }
        }
        public void InsertOneUserResult(int idUser, int idTest, int result)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_queryInsertResutTesting, connection))
                {
                    command.Parameters.Add("@idUser", SqlDbType.Int).Value = idUser;
                    command.Parameters.Add("@idTest", SqlDbType.Int).Value = idTest;
                    command.Parameters.Add("@testResult", SqlDbType.Int).Value = result;
                    command.Parameters.Add("@DateOfResult", SqlDbType.DateTime).Value = DateTime.Now;
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
        public bool IsOptionCorrect(int idOption)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_queryIsOptionCorrect, connection))
                {
                    command.Parameters.Add("@idOption", SqlDbType.Int).Value = idOption;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        bool isCorrect = (bool)reader["IsCorect"];
                        return isCorrect;
                    }
                }
            }
        }
        public int SelectUserIdByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(_querySelectUserIdByEmail, connection))
                {
                    command.Parameters.Add("@email", SqlDbType.NVarChar, 255).Value = email;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int userId;
                        reader.Read();
                        userId = (int)reader["Id"];
                        return userId;
                    }
                }
            }
        }
        public List<UserResultEntity> GetResultsUserByUserId(int idUser)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(_querySelectUserResultsByUserId, connection))
                {
                    command.Parameters.Add("@idUser", SqlDbType.Int, 255).Value = idUser;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<UserResultEntity> selectedUserResults = new List<UserResultEntity>();
                        while (reader.Read())
                        {
                            selectedUserResults.Add(new UserResultEntity() {
                                Id = (int)reader["Id"],
                                IdTest = (int)reader["IdTest"],
                                IdUser = (int)reader["IdUser"],
                                TestResult = (int)reader["TestResult"],
                                DateOfResult = (DateTime)reader["DateOfResult"]
                            });
                        }
                        return selectedUserResults;
                    }
                }
            }
        }

        public UserEntity GetUserByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_queryGetUserByEmail, connection))
                {
                    command.Parameters.Add("@email", SqlDbType.NVarChar, 255).Value = email;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        UserEntity selectedUser = new UserEntity();
                        reader.Read();
                        if (reader.HasRows)
                        {
                            selectedUser.Id = (int)reader["Id"];
                            selectedUser.FirstName = (string)reader["FirstName"];
                            selectedUser.LastName = (string)reader["LastName"];
                            selectedUser.Email = (string)reader["Email"];
                            selectedUser.Password = (string)reader["Password"];
                            return selectedUser;
                        }
                        return selectedUser;
                    }
                }
            }
        }
    }
}
