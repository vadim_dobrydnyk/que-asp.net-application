﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Que.Entities;

namespace Que.Repositories
{
    public interface ITestRepository
    {
        List<TestEntity> GetAllTests();
        TestEntity GetTestById(int id);
        bool IsOptionCorrect(int idOption);
        void InsertOneUserResult(int idUser, int idTest, int result);
        int SelectUserIdByEmail(string email);
        List<UserResultEntity> GetResultsUserByUserId(int idUser);
        UserEntity GetUserByEmail(string email);
    }
}
