﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Que.Entities;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Que.Repositories
{
    public class SqlUserRepository : IUserRepository
    {
        private string _connectionString;

        private string _queryGetUserByEmail = @"SELECT [Id],[FirstName],[LastName],[Email],[Password] FROM tblUsers WHERE [Email] = @email";
        private string _queryInsertUser = @"INSERT INTO tblUsers ([FirstName], [LastName], [Email], [Password]) VALUES(@FirstName, @LastName, @Email, @Password)";


        public SqlUserRepository(string stringConnection)
        {
            _connectionString = stringConnection;
        }

        public UserEntity GetUserByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_queryGetUserByEmail, connection))
                {
                    command.Parameters.Add("@email", SqlDbType.NVarChar, 255).Value = email;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        UserEntity selectedUser = new UserEntity();
                        reader.Read();
                        if (reader.HasRows)
                        {
                            selectedUser.Id = (int)reader["Id"];
                            selectedUser.FirstName = (string)reader["FirstName"];
                            selectedUser.LastName = (string)reader["LastName"];
                            selectedUser.Email = (string)reader["Email"];
                            selectedUser.Password = (string)reader["Password"];
                            return selectedUser;
                        }
                        return selectedUser;
                    }
                }
            }
        }
        public void AddUser(string firstName, string lastName, string email, string password)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_queryInsertUser, connection))
                {
                    command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 255).Value = firstName;
                    command.Parameters.Add("@LastName", SqlDbType.NVarChar, 255).Value = lastName;
                    command.Parameters.Add("@Email", SqlDbType.NVarChar, 255).Value = email;
                    command.Parameters.Add("@Password", SqlDbType.NVarChar, 255).Value = password;
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
    }
}
