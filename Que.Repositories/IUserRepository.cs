﻿using Que.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Que.Repositories
{
    public interface IUserRepository
    {
        UserEntity GetUserByEmail(string email);
        void AddUser(string firstName, string lastName, string email, string password);
    }
}
