﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Que.Entities
{
    public class OptionEntity
    {
        public int Id { get; set; }
        public int IdQuestion { get; set; }
        public string Title { get; set; }
    }
}
