﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Que.Entities
{
    public class QuestionEntity
    {
        public int Id { get; set; }
        public int IdTest { get; set; }
        public string Title { get; set; }
        public List<OptionEntity> Options { get; set; }
    }
}
