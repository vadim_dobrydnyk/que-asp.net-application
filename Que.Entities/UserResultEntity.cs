﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Que.Entities
{
    public class UserResultEntity
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdTest { get; set; }
        public int TestResult { get; set; }
        public DateTime DateOfResult { get; set; }
    }
}
