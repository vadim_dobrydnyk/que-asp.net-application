use Que

SET IDENTITY_INSERT tblTests ON

INSERT INTO tblTests
				([Id], [TestName])
			VALUES
				(1, 'C Sharp (C#)'),
				(2, 'JavaScript (JS)')

SET IDENTITY_INSERT tblTests OFF

SET IDENTITY_INSERT tblQuestions ON

INSERT INTO tblQuestions
				([Id],[IdTest], [Title])
			VALUES
				(1, 1, 'Some c# que 1'),
				(2, 1, 'Some c# que 2'),
				(3, 1, 'Some c# que 3'),
				(4, 1, 'Some c# que 4'),
				(5, 1, 'Some c# que 5'),
				(6, 1, 'Some c# que 6'),
				(7, 1, 'Some c# que 7'),
				(8, 1, 'Some c# que 8'),
				(9, 1, 'Some c# que 9'),
				(10, 1, 'Some c# que 10'),
				(11, 2, 'Some js que 1'),
				(12, 2, 'Some js que 2'),
				(13, 2, 'Some js que 3'),
				(14, 2, 'Some js que 4'),
				(15, 2, 'Some js que 5'),
				(16, 2, 'Some js que 6'),
				(17, 2, 'Some js que 7'),
				(18, 2, 'Some js que 8'),
				(19, 2, 'Some js que 9'),
				(20, 2, 'Some js que 10')

SET IDENTITY_INSERT tblQuestions OFF

SET IDENTITY_INSERT tblOptions ON

INSERT INTO tblOptions
				([Id], [IdQuestion], [Title], [IsCorect])
			VALUES
				(1, 1, 'Some option for c# que 1', 1),
				(2, 1, 'Some option for c# que 1', 0),
				(3, 1, 'Some option for c# que 1', 0),
				(4, 2, 'Some option for c# que 2', 1),
				(5, 2, 'Some option for c# que 2', 0),
				(6, 2, 'Some option for c# que 2', 0),
				(7, 3, 'Some option for c# que 3', 1),
				(8, 3, 'Some option for c# que 3', 0),
				(9, 3, 'Some option for c# que 3', 0),
				(10, 4, 'Some option for c# que 4', 1),
				(11, 4, 'Some option for c# que 4', 0),
				(12, 4, 'Some option for c# que 4', 0),
				(13, 5, 'Some option for c# que 5', 1),
				(14, 5, 'Some option for c# que 5', 0),
				(15, 5, 'Some option for c# que 5', 0),
				(16, 6, 'Some option for c# que 6', 1),
				(17, 6, 'Some option for c# que 6', 0),
				(18, 6, 'Some option for c# que 6', 0),
				(19, 7, 'Some option for c# que 7', 1),
				(20, 7, 'Some option for c# que 7', 0),
				(21, 7, 'Some option for c# que 7', 0),
				(22, 8, 'Some option for c# que 8', 1),
				(23, 8, 'Some option for c# que 8', 0),
				(24, 8, 'Some option for c# que 8', 0),
				(25, 9, 'Some option for c# que 9', 1),
				(26, 9, 'Some option for c# que 9', 0),
				(27, 9, 'Some option for c# que 9', 0),
				(28, 10, 'Some option for c# que 10', 1),
				(29, 10, 'Some option for c# que 10', 0),
				(30, 10, 'Some option for c# que 10', 0),
				(31, 11, 'Some option for js que 1', 1),
				(32, 11, 'Some option for js que 1', 0),
				(33, 11, 'Some option for js que 1', 0),
				(34, 12, 'Some option for js que 2', 1),
				(35, 12, 'Some option for js que 2', 0),
				(36, 12, 'Some option for js que 2', 0),
				(37, 13, 'Some option for js que 3', 1),
				(38, 13, 'Some option for js que 3', 0),
				(39, 13, 'Some option for js que 3', 0),
				(40, 14, 'Some option for js que 4', 1),
				(41, 14, 'Some option for js que 4', 0),
				(42, 14, 'Some option for js que 4', 0),
				(43, 15, 'Some option for js que 5', 1),
				(44, 15, 'Some option for js que 5', 0),
				(45, 15, 'Some option for js que 5', 0),
				(46, 16, 'Some option for js que 6', 1),
				(47, 16, 'Some option for js que 6', 0),
				(48, 16, 'Some option for js que 6', 0),
				(49, 17, 'Some option for js que 7', 1),
				(50, 17, 'Some option for js que 7', 0),
				(51, 17, 'Some option for js que 7', 0),
				(52, 18, 'Some option for js que 8', 1),
				(53, 18, 'Some option for js que 8', 0),
				(54, 18, 'Some option for js que 8', 0),
				(55, 19, 'Some option for js que 9', 1),
				(56, 19, 'Some option for js que 9', 0),
				(57, 19, 'Some option for js que 9', 0),
				(58, 20, 'Some option for js que 10', 1),
				(59, 20, 'Some option for js que 10', 0),
				(60, 20, 'Some option for js que 10', 0)

SET IDENTITY_INSERT tblOptions OFF

SET IDENTITY_INSERT tblQuestionAnswer ON

INSERT INTO tblQuestionAnswer
				([Id], [IdQuestion], [IdOption])
			VALUES
				(1, 1, 2),
				(2, 2, 5),
				(3, 3, 9),
				(4, 3, 12)

SET IDENTITY_INSERT tblQuestionAnswer OFF


SET IDENTITY_INSERT tblUsers ON

INSERT INTO tblUsers
				([Id],[FirstName],[LastName],[Email],[Password])
			VALUES
				(1, 'User', 'user', 'email', 'email')

SET IDENTITY_INSERT tblUsers OFF

