CREATE DATABASE Que

go

use Que

CREATE TABLE tblUsers (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[FirstName] nvarchar(255) not null,
	[LastName] nvarchar(255) not null,
	[Email] nvarchar(255) not null,
	[Password] nvarchar(255) null 
);
CREATE TABLE tblUsersAndResults (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[IdUser] int not null,
	[IdTest] int not null,
	[TestResult] int not null,
	[DateOfResult] datetime not null
);
CREATE TABLE tblTests (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[TestName] nvarchar(64) not null,
);

CREATE TABLE tblQuestions (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[IdTest] int not null,
	[Title] nvarchar(255) not null
);

CREATE TABLE tblOptions (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[IdQuestion] int not null,
	[Title] nvarchar(255) not null,
	[IsCorect] bit not null
);

CREATE TABLE tblQuestionAnswer (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[IdQuestion] int not null,
	[IdOption] int not null
);