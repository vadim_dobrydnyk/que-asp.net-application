﻿using Que.Entities;
using Que.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Que.WebUI.Code
{
    public class FormsSecurityManager : ISecurityManager
    {
        private IUserRepository _userRepository;

        public FormsSecurityManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public bool Login(string userName, string password)
        {
            UserEntity user = _userRepository.GetUserByEmail(userName);
            if (user.Email == userName && user.Password == password)
            {
                FormsAuthentication.SetAuthCookie(user.Email, false);
                return true;
            }

            return false;
        }
        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}