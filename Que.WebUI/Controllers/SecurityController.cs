﻿using Que.Repositories;
using Que.WebUI.Code;
using Que.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Que.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private readonly ISecurityManager _securityManger;
        private readonly IUserRepository _userRepository;

        public SecurityController(ISecurityManager securityManger, IUserRepository userRepository)
        {
            _securityManger = securityManger;
            _userRepository = userRepository;
        }

        [HttpGet]
        public ActionResult Login()
        {
            if(Request.IsAuthenticated)
            {
                RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(SecurityModel securityModel)
        {
            if (_securityManger.Login(securityModel.Login, securityModel.Password))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Logout()
        {
            _securityManger.Logout();
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult Register()
        {
            if (Request.IsAuthenticated)
            {
                RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Register(string firstName, string lastName, string email, string password)
        {
            _userRepository.AddUser(firstName, lastName, email, password);
            return RedirectToAction("Index", "Home");
        }
    }
}