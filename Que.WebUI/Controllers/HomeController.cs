﻿using Que.Repositories;
using Que.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Que.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITestRepository _repository;

        public HomeController(ITestRepository repository)
        {
            _repository = repository;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserRoom()
        {
            if (Request.IsAuthenticated)
            {
                string userLogin = HttpContext.User.Identity.Name;

                var UserRoom = new UserRoomModel();

                UserRoom.UserData = _repository.GetUserByEmail(userLogin);
                UserRoom.UserResults = new List<UserResults>();

                var UserResultsEntity = _repository.GetResultsUserByUserId(UserRoom.UserData.Id);

                UserResults userResult = new UserResults();

                foreach (var ure in UserResultsEntity)
                {
                    var testName = _repository.GetTestById(ure.IdTest).Title;
                    var testResult = ure.TestResult;
                    var idTest = ure.IdTest;
                    var dateOfResult = ure.DateOfResult;
                    UserRoom.UserResults.Add(new UserResults() { TestName = testName, TestResult = testResult, IdTest = idTest, DateOfResult = dateOfResult});
                }
                return View(UserRoom);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}