﻿using Que.Entities;
using Que.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Que.WebUI.Controllers
{
    [Authorize]
    public class TestController : Controller
    {
        private ITestRepository _repository;

        public TestController(ITestRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult SelectTest()
        {
            List<TestEntity> tests = _repository.GetAllTests();
            ViewBag.test = tests;
            return View();
        }

        [HttpPost]
        public ActionResult SelectTest(int idTest)
        {
            return RedirectToAction("Testing", "Test", new { idTest = idTest });
        }

        [HttpGet]
        public ActionResult Testing(int idTest)
        {
            TestEntity testItem = _repository.GetTestById(idTest);
            ViewBag.test = testItem;
            return View();
        }

        [HttpPost]
        public ActionResult Testing(int idTest, int[] checkedIdOptions = null)
        {
            checkedIdOptions = checkedIdOptions ?? new int[0];
            int totalTestResult = 0;
            foreach(var id in checkedIdOptions)
            {
                if(_repository.IsOptionCorrect(id))
                {
                    totalTestResult += 1;
                }
            }
            string userLogin = HttpContext.User.Identity.Name;
            int userId = _repository.SelectUserIdByEmail(userLogin);

            _repository.InsertOneUserResult(userId, idTest, totalTestResult);
            return PartialView("TestCompleted", totalTestResult);
        }
    }
}