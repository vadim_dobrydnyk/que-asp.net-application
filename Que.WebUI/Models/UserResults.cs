﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Que.WebUI.Models
{
    public class UserResults
    {
        public int IdTest { get; set; }
        public string TestName { get; set; }
        public int TestResult { get; set; }
        public DateTime DateOfResult { get; set; }
    }
}