﻿using Que.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Que.WebUI.Models
{
    public class UserRoomModel
    {
        public UserEntity UserData { get; set; }
        public List<UserResults> UserResults { get; set; }
    }
}