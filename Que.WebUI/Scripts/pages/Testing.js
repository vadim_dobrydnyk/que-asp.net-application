﻿(function () {
    var sendResult = function () {
        var allRadio = $('input[optionId]:checked');
        var chechedRadio = [];
        for(var i = 0; i < allRadio.length; i++) {
            chechedRadio.push($(allRadio[i]).attr('optionId'));
        };
        var idTest = $('input[name="testId"]').attr("value");
        $.ajax({
            url: "/Test/Testing",
            type: "POST",
            dataType: "text",
            data: {
                checkedIdOptions: chechedRadio,
                idTest: idTest
            }
        }).done(function (TestCompleted) {
            console.log("done");
            $(".panel, .panel-default").html(TestCompleted);
        });
    };
     

    $(function () {
        $('button[name="submite-result"]').on("click", sendResult);
    })
})();