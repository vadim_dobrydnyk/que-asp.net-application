using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using System.Configuration;
using Que.Repositories;
using Que.WebUI.Code;

namespace Que.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            var connectionString = ConfigurationManager.ConnectionStrings["Que"].ConnectionString;
            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUserRepository, SqlUserRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ITestRepository, SqlTestRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ISecurityManager, FormsSecurityManager>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}